execute 'install epel-release' do
  command 'amazon-linux-extras install -y epel'
  action :run
end

package 'clamav' do
  action :install
end

package 'amazon-efs-utils' do
  action :install
end

cookbook_file '/etc/freshclam.conf' do
  source 'etc/freshclam.conf'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

cookbook_file '/etc/clamd.d/scan.conf' do
  source 'etc/clamd.d/scan.conf'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

cookbook_file '/root/efs-mount.sh' do
  source 'root/efs-mount.sh'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

directory '/mnt/efs' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end
