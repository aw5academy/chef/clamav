#!/bin/bash

EFS_ID="$(cat /etc/efs_id)"
MAX_MOUNT_ATTEMPTS=10
WAIT_TIME_BETWEEN_ATTEMPTS=60

MOUNT_ATTEMPT=$(($MOUNT_ATTEMPT + 1))
while [ $MOUNT_ATTEMPT -le $MAX_MOUNT_ATTEMPTS ]; do
  mount -t efs -o tls $EFS_ID:/ /mnt/efs
  if [ ! "`df -h |grep efs`" ]; then
    printf "EFS not yet mounted. Waiting for ${WAIT_TIME_BETWEEN_ATTEMPTS}s until next attempt...\n"
    MOUNT_ATTEMPT=$(($MOUNT_ATTEMPT + 1))
    sleep $WAIT_TIME_BETWEEN_ATTEMPTS
  else
    exit
  fi
done

printf "Could not mount EFS after $MAX_MOUNT_ATTEMPTS attempts with ${WAIT_TIME_BETWEEN_ATTEMPTS}s between attempts!\n"
